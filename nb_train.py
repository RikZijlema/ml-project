#!/user/bin/python3
#Final project Machine Learning Project

import pandas
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.model_selection import train_test_split, StratifiedKFold
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import accuracy_score,recall_score, precision_score, f1_score, precision_recall_fscore_support
import numpy as np


def get_data():
    data = pandas.read_csv(open("train.csv"), sep=';')
    data.columns = ['label','tweet']
    labels = data['label']
    tweets = data['tweet']

    return labels, tweets

def init_vectorizer(labels, tweets):
    tfidf_vect = TfidfVectorizer(ngram_range=(1,1))
    X = tfidf_vect.fit_transform(tweets)
    y = labels

    return X, y
    

def cross_validation(X,y):
    cv = StratifiedKFold(n_splits=10, random_state=None, shuffle = False)

    accuracies = list()
    precisions = list()
    recalls = list()
    f_scores = list()
    POS = list()
    NEG = list()

    X = X.toarray()
    for train_idx, test_idx in cv.split(X,y):
        X_train, X_test, y_train, y_test = X[train_idx], X[test_idx], y[train_idx], y[test_idx]
        classifier = MultinomialNB().fit(X_train, y_train)
        y_pred = classifier.predict(X_test)

        #calculate scores
        accuracy = accuracy_score(y_test, y_pred)
        recall = recall_score(y_test,y_pred, average='macro')
        precision = precision_score(y_test,y_pred, average='macro')
        f_score = f1_score(y_test,y_pred, average='macro')
        per_class = precision_recall_fscore_support(y_test,y_pred, labels=["positive","negative"])

        #append scores to lists
        accuracies.append(accuracy)
        precisions.append(precision)
        recalls.append(recall)
        f_scores.append(f_score)
        POS.append(per_class[2][0])
        NEG.append(per_class[2][1])

    #print averages of lists
    print("Average accuracy: ",np.mean(accuracies))
    print("Average precision: ",np.mean(precisions))
    print("Average recall: ",np.mean(recalls))
    print("Average F-Score: ",np.mean(f_scores))
    print("Average POS F-score: ",np.mean(POS))
    print("Average NEG F-score: ",np.mean(NEG))


def main():

    labels, tweets = get_data()
    X, y = init_vectorizer(labels, tweets)
    cross_validation(X,y)
    


if __name__ == '__main__':
    main()