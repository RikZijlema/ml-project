#!/usr/bin/env python3
# SVM for training

import pandas
from sklearn.svm import LinearSVC
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.model_selection import train_test_split, StratifiedKFold
from sklearn.metrics import accuracy_score,recall_score, precision_score, f1_score, precision_recall_fscore_support
from sklearn import metrics
import numpy as np

def main():
    """Open the dataset"""
    data = pandas.read_csv(open("nrc_mpqa_emoti_sets/train_nrc_mpqa_emoti.csv"), sep=';')
    data.columns = ["label","tweet","nrc_positive","nrc_negative","mpqa_positive","mpqa_negative","emo_positive","emo_negative"]
    label = data["label"]
    tweets = data["tweet"]

    features = data[["nrc_positive","nrc_negative","mpqa_positive","mpqa_negative","emo_positive","emo_negative"]]
    X = np.array(features)
    y = label
    cv = StratifiedKFold(n_splits=10, random_state=None, shuffle = False)
    """Lists that will contain 10 values after 10-fold cross-validation"""
    accuracy_list = []
    precision_list = []
    recall_list = []
    f_score_list = []
    POS_list = []
    NEG_list = []
    """10-Fold cross-validation"""
    for train_index, test_index in cv.split(X,y):
        X_train, X_test, y_train, y_test = X[train_index], X[test_index], y[train_index], y[test_index]
        clf = LinearSVC().fit(X_train, y_train)
        y_pred = clf.predict(X_test)
        """Evaluation"""
        accuracy = accuracy_score(y_test, y_pred)
        recall = recall_score(y_test,y_pred, average='macro')
        precision = precision_score(y_test,y_pred, average='macro')
        f_score = f1_score(y_test,y_pred, average='macro')
        per_class = precision_recall_fscore_support(y_test,y_pred, labels=["positive","negative"])
        accuracy_list.append(accuracy)
        precision_list.append(precision)
        recall_list.append(recall)
        f_score_list.append(f_score)
        POS_list.append(per_class[2][0])
        NEG_list.append(per_class[2][1])
    """Calculating the averages of the lists"""
    avg_accuracy = np.mean(accuracy_list)
    avg_precision = np.mean(precision_list)
    avg_recall = np.mean(recall_list)
    avg_f_score = np.mean(f_score_list) 
    avg_POS = np.mean(POS_list)
    avg_NEG = np.mean(NEG_list)
    """Printing the averages"""
    print("Average accuracy: ",avg_accuracy)
    print("Average precision: ",avg_precision)
    print("Average recall: ",avg_recall)
    print("Average F-Score: ",avg_f_score)
    print("Average POS F-score: ",avg_POS)
    print("Average NEG F-score: ",avg_NEG) 

if __name__ == "__main__":
    main()

