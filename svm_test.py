#!/usr/bin/env python3
# SVM for testing

import pandas
from sklearn.svm import LinearSVC
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.metrics import accuracy_score,recall_score, precision_score, f1_score, precision_recall_fscore_support
from sklearn import metrics
import numpy as np

def read_dataset():
    """Opening the data sets"""
    training_data = pandas.read_csv(open("train.csv"), sep=';')
    testing_data = pandas.read_csv(open("test.csv"), sep=';')
    testing_data.columns = ["label","tweet"]
    testing_tweets = testing_data["tweet"]
    testing_labels = testing_data["label"]
    training_data.columns = ["label","tweet"]
    tweets = training_data["tweet"]
    labels = training_data["label"]
    return(testing_tweets,testing_labels,tweets,labels)

def init_vect(testing_tweets,testing_labels,tweets,labels):
    """Initialize vectorizer"""
    tfidf_vect = TfidfVectorizer(ngram_range=(1,2))
    X_train = tfidf_vect.fit_transform(tweets)
    X_test = tfidf_vect.transform(testing_tweets)
    y_train = labels
    y_test = testing_labels
    return(X_train,X_test,y_train,y_test)
    
def classification(X_train,y_train,X_test):
    clf = LinearSVC().fit(X_train, y_train)
    y_pred = clf.predict(X_test)
    return(y_pred)

def evaluation(y_test,y_pred):
    accuracy = accuracy_score(y_test, y_pred)
    recall = recall_score(y_test,y_pred, average='macro')
    precision = precision_score(y_test,y_pred, average='macro')
    f_score = f1_score(y_test,y_pred, average='macro')
    per_class = precision_recall_fscore_support(y_test,y_pred, labels=["positive","negative"])
    """Printing the results"""
    print("Accuracy: ",accuracy)
    print("Recall: ",recall)
    print("Precision: ",precision)
    print("F-Score: ",f_score)
    print("POS F-Score: ",per_class[2][0])
    print("NEG F-Score: ",per_class[2][1])

def main():
    testing_tweets,testing_labels,tweets,labels = read_dataset()
    X_train,X_test,y_train,y_test = init_vect(testing_tweets,testing_labels,tweets,labels)
    y_pred = classification(X_train,y_train,X_test)
    evaluation(y_test,y_pred)

if __name__ == "__main__":
    main()

