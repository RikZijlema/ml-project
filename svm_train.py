#!/usr/bin/env python3
# SVM for training

import pandas
from sklearn.svm import LinearSVC
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.model_selection import train_test_split, StratifiedKFold
from sklearn.metrics import accuracy_score,recall_score, precision_score, f1_score, precision_recall_fscore_support
from sklearn import metrics
from scipy.sparse import hstack
import numpy as np

def read_dataset():
   """Open the dataset"""
   data = pandas.read_csv(open("mpqa_sets/train_mpqa.csv"), sep=';')
   data.columns = ["label","tweet","mpqa_pos","mpqa_neg"]
   features = data[["mpqa_pos","mpqa_neg"]]
   label = data["label"]
   tweets = data["tweet"]
   return(label,tweets,features)

def init_vect(tweets,label,features):
    """Initialize vectorizer"""
    tfidf_vect = TfidfVectorizer(ngram_range=(1,2))
    tfidf_matrix = tfidf_vect.fit_transform(tweets)
    tfidf_dataframe = pandas.DataFrame.sparse.from_spmatrix(tfidf_matrix)
    new_tfidf = pandas.concat((tfidf_dataframe,features), axis=1) 
    X = np.array(new_tfidf)
    y = label
    return(X,y)


def cross_validation(X,y):
    cv = StratifiedKFold(n_splits=10, random_state=None, shuffle = False)

    """Lists that will contain 10 values after 10-fold cross-validation"""
    accuracy_list = []
    precision_list = []
    recall_list = []
    f_score_list = []
    POS_list = []
    NEG_list = []

    """10-Fold cross-validation"""
    for train_index, test_index in cv.split(X,y):
        X_train, X_test, y_train, y_test = X[train_index], X[test_index], y[train_index], y[test_index]
        clf = LinearSVC().fit(X_train, y_train)
        y_pred = clf.predict(X_test)
        """Evaluation"""
        accuracy = accuracy_score(y_test, y_pred)
        recall = recall_score(y_test,y_pred, average='macro')
        precision = precision_score(y_test,y_pred, average='macro')
        f_score = f1_score(y_test,y_pred, average='macro')
        per_class = precision_recall_fscore_support(y_test,y_pred, labels=["positive","negative"])
        accuracy_list.append(accuracy)
        precision_list.append(precision)
        recall_list.append(recall)
        f_score_list.append(f_score)
        POS_list.append(per_class[2][0])
        NEG_list.append(per_class[2][1])

    """Calculating the averages of the lists"""
    avg_accuracy = np.mean(accuracy_list)
    avg_precision = np.mean(precision_list)
    avg_recall = np.mean(recall_list)
    avg_f_score = np.mean(f_score_list) 
    avg_POS = np.mean(POS_list)
    avg_NEG = np.mean(NEG_list)

    """Printing the averages"""
    print("Average accuracy: ",avg_accuracy)
    print("Average precision: ",avg_precision)
    print("Average recall: ",avg_recall)
    print("Average F-Score: ",avg_f_score)
    print("Average POS F-score: ",avg_POS)
    print("Average NEG F-score: ",avg_NEG) 

def main():
    label,tweets,features = read_dataset()
    X,y = init_vect(tweets,label,features)
    cross_validation(X,y)

if __name__ == "__main__":
    main()

