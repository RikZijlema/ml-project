#!/usr/bin/env python3
# Script used to add the MPQA sentiment weights

import pandas

def main():
    """Open data set"""
    data = pandas.read_csv(open("mpqa_sentiment.csv"), sep=' ', header=None)
    data.columns = ["word","sentiment"]
    tweets = pandas.read_csv(open("test.csv"), sep=';', header=None)
    tweets.columns = ["label","tweet"]
    tweet = tweets["tweet"]
    sen_word = data["word"]
    positive_list = []
    negative_list = []
    """Iterate over each tweet"""
    for tweet in tweet:
        print(tweet)
        positive_count = 0
        negative_count = 0
        """Iterates over each word of the tweet"""
        for word in tweet.split():
            word = word.lower()
            word = word.rstrip("!.,?",)
            """Iterates over the English translation of NRC lexicon"""
            for row in data.itertuples():
                sentiment_word = row[1]
                sentiment = row[2]
                """If word in tweet corresponds to word in row"""
                if word == sentiment_word:
                    if sentiment == "positive":
                        positive_count += 1
                    elif sentiment == "negative":
                        negative_count += 1
                    else:
                        pass
            else:
                pass
        """Calculation of the sentiment weights"""
        tweet_length = len(tweet.split())
        positive_list.append((positive_count/tweet_length))
        negative_list.append((negative_count/tweet_length))

    """Creates a file containing the sentiment weights for each tweet"""
    tweets["positive"] = positive_list
    tweets["negative"] = negative_list

    tweets.to_csv("test_mpqa.csv", sep=";")

if __name__ == "__main__":

    main()

