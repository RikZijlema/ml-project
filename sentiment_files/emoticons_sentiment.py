#!/usr/bin/env python3
# Script used to add the NRC sentiment weights

import pandas

def main():
    """Open data set"""
    tweets = pandas.read_csv(open("test.csv"), sep=';', header=None)
    tweets.columns = ["label","tweet"]
    tweet = tweets["tweet"]
    positive_list = []
    negative_list = []
    positive_emoticons = [":)",":-)",":D",";)"]
    negative_emoticons = [":(",">:(",">:-("]
    """Iterate over each tweet"""
    for tweet in tweet:
        print(tweet)
        positive_count = 0
        negative_count = 0
        """Iterates over each word of the tweet"""
        for word in tweet.split():
            word = word.lower()
            if word in positive_emoticons:
                positive_count += 1
            if word in negative_emoticons:
                negative_count += 1

        positive_list.append(positive_count)
        negative_list.append(negative_count)

    """Creates a file containing the emoticon counts for each tweet"""
    tweets["positive"] = positive_list
    tweets["negative"] = negative_list

    tweets.to_csv("test_emoti.csv", sep=";")

if __name__ == "__main__":

    main()

